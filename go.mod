module modernc.org/parser

go 1.18

require (
	modernc.org/golex v1.1.0
	modernc.org/scanner v1.1.0
	modernc.org/strutil v1.2.0
)
